<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name'           => 'admin',
        'email'          => 'admin@admin.com',
        'password'       => bcrypt('123123123'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Post::class, function (Faker\Generator $faker) {

    return [
        'slug'    => $faker->slug(),
        'title'   => $faker->company,
        'image'   => $faker->image(public_path('images'), 100, 100, 'transport', false),
        'preview' => $faker->realText(140),
        'text'    => $faker->realText(600),
    ];
});
