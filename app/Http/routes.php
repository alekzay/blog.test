<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses' => 'PostController@index', 'as' => 'blog.index']);

Route::resource('blog', 'PostController', ['only' => ['show']]);

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::any('/', function () {
        return redirect()->route('admin.post.index');
    });

    Route::resource('post', 'PostController');
});

Route::auth();