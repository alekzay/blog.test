<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Image;

/**
 * @property string image
 */
class Post extends Model
{
    protected $guarded = [];

    public function setImageAttribute($file)
    {
        if (is_string($file)) {
            $this->attributes['image'] = $file;
        } elseif ($file instanceof UploadedFile) {
            $image = Image::make($file);
            if($image->width() > $image->height()) {
                $image->resize(100, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image->resize(null, 100, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $filename  = time() . '.' . $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
            $image->save(public_path('images/' . $filename));
            $this->attributes['image'] = $filename;
        }
    }
}
