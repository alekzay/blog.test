@extends('blog.layout')

@section('content')

    <div class="row">
        @foreach($posts as $post)
            <div class="col-md-10">
                <div class="media">
                    <div class="media-left media-middle">
                        <a href="{{ route('blog.show', $post->slug) }}">
                            <img class="media-object" src="{{ asset('images/' . $post->image) }}" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            <a href="{{ route('blog.show', $post->slug) }}">
                                {{ $post->title }}
                            </a>
                        </h4>
                        <span class="label label-primary">{{ $post->updated_at }}</span>
                        <p>
                            {{ $post->preview }}
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-2">

                <form class="form-inline" method="post" action="{{ route('admin.post.destroy', ['id' => $post->id]) }}">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <a class="btn btn-warning" href="{{ route('admin.post.edit', ['id' => $post->id]) }}">
                        edit
                    </a>
                    <button type="submit" class="btn btn-danger">
                        delete
                    </button>
                </form>
            </div>
        @endforeach
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-md-12">
            <p>
                <a class="btn btn-success" href="{{ route('admin.post.create') }}">New post</a>
            </p>
        </div>
    </div>

@endsection
