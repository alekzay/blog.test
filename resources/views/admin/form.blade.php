@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{{ csrf_field() }}

<div class="form-group">
    <label>slug</label>
    <input name="slug" type="text" class="form-control" id="" placeholder="slug" value="{{ $post->slug }}">
</div>

<div class="form-group">
    <label>title</label>
    <input name="title" type="text" class="form-control" id="" placeholder="title" value="{{ $post->title }}">
</div>

<div class="form-group">
    <label>preview</label>
    <textarea name="preview" class="form-control">{{ $post->preview }}</textarea>
</div>

<div class="form-group">
    <label>text</label>
    <textarea name="text" class="form-control">{{ $post->text }}</textarea>
</div>

<div class="form-group">
    <label>text</label>
    <input type="file" name="image">
</div>

<div class="form-group">
    <button class="btn btn-success" type="submit">save</button>
</div>