@extends('blog.layout')

@section('content')

    <form action="{{ route('admin.post.store') }}" method="post" enctype="multipart/form-data">

        @include('admin.form')

    </form>

@endsection