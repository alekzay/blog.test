@extends('blog.layout')

@section('content')

    <form action="{{ route('admin.post.update', ['id' => $post->id]) }}" method="post" enctype="multipart/form-data">

        {{ method_field('PATCH') }}

        @include('admin.form')

    </form>

@endsection