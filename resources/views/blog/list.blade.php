@extends('blog.layout')

@section('content')

        @foreach($posts as $post)
            <div class="media">
                <div class="media-left media-middle">
                    <a href="{{ route('blog.show', $post->slug) }}">
                        <img class="media-object" src="{{ asset('images/' . $post->image) }}" alt="">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">
                        <a href="{{ route('blog.show', $post->slug) }}">
                            {{ $post->title }}
                        </a>
                    </h4>
                    <span class="label label-primary">{{ $post->updated_at }}</span>
                    <p>
                        {{ $post->preview }}
                    </p>
                </div>
            </div>
        @endforeach

@endsection
