<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
    <link rel="stylesheet" href="/css/all.css">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Blog</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/admin">admin</a></li>
            </ul>
            @if(Auth::user())
                <div class="navbar-right">
                    <a class="navbar-brand pull-right" href="/logout">logout</a>
                </div>
            @endif
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">

    @yield('content', '')

</div><!-- /.container -->


<script src="/js/all.js"></script>
</body>
</html>