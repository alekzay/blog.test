@extends('blog.layout')

@section('content')

    <div class="media">
        <div class="media-left media-middle">
            <img class="media-object" src="{{ asset('images/' . $post->image) }}" alt="">
        </div>
        <div class="media-body">
            <h1 class="media-heading">
                {{ $post->title }}
            </h1>
            <span class="label label-primary">{{ $post->updated_at }}</span>
            <p>
                {{ $post->text }}
            </p>
        </div>
    </div>

    <a href="{{ route('blog.index') }}">
        all posts
    </a>

@endsection
